const coll = document.getElementsByClassName("collapsible");
const arrow = document.getElementsByClassName("arrow");
let i;

for (i = 0; i < coll.length; i++) {
    coll[i].addEventListener("click", function () {
        this.classList.toggle("active");
        const arrow = this.querySelector('.arrow');
        const content = this.nextElementSibling;
        if (content.style.maxHeight) {
            content.style.maxHeight = null;
            content.style.marginBottom = "0px";
            arrow.style.transform = 'rotate(0)';
            this.style.borderBottom = '1px solid #F3EAE5';
        } else {
            content.style.maxHeight = content.scrollHeight + "px";
            content.style.marginBottom = "8px";
            arrow.style.transform = 'rotate(180deg)';
            this.style.borderBottom = 'none'
        }
    });
}